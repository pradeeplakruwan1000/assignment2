
public class MultiLinugualStringTable {
  private enum LanguageSetting {English, Klingon}
  private static LanguageSetting cl = LanguageSetting.English;
  private static String [] em = {"Enter your name:", "Welcome", "Have a good time playing Abominodo"};
  private static String [] km = {"'el lIj pong:", "nuqneH", "QaQ poH Abominodo"};

public static String getMessage(int index) {
  switch (currentLanguage) {
    case ENGLISH:
      return englishMessages[index];
    case KLINGON:
      return klingonMessages[index];
    default:
      return ""; // Handle unknown language setting
    }
  }
}
